import os
import shutil
import glob

def setup_folders(filePath=None, folderName=None):
    if filePath == None:
        if folderName == None:
            simuFolder = "../results"
        else:
            simuFolder = "../" + folderName
    else:
        if folderName == None:
            simuFolder = filePath
        else:
            if filepath[-1] == '/':
                simuFolder = filePath + folderName
            else:
                simuFolder = filePath + '/' + folderName
    try:
        os.mkdir(simuFolder)
    except OSError as error:
        print(error)
        print("Replacing this folder with a newer version.")
        shutil.rmtree(simuFolder)
        os.mkdir(simuFolder)
    finally:
        os.mkdir(simuFolder + "/data")
        os.mkdir(simuFolder + "/code")
        os.mkdir(simuFolder + "/graphs")
        for file in glob.glob("*.py"):
            shutil.copy(file, simuFolder+"/code")

