#-- std libraries
from numba import jit, njit
import numpy as np
from math import pi
#-- my files
import parameters as par
import PDE_timeStep as pde
import functions as f

#-==== Test parameters ====-#
p = np.array([2, 3, 1]) #determines exact solution phi (should be integers)
q = np.array([3, 4, 0]) #determines exact solution v (should be integers)

#-==== eigenvalues associated to the test functions ====-#
l0 = (1./par.H)*par.oc*par.oe/(par.oe+par.oc)
if q[1]**2 + q[2]**2 > 0:
    l1 = (2.*pi*par.H/par.L)*(q[1]**2 + q[2]**2)**0.5
    l1 = (1./par.H)*par.oc*par.oe/(par.oe+par.oc)*l1/np.tanh(l1)
else:
    l1 = l0

#-==== Exact solutions ====-#
@njit
def phi_exact(t,x,y):
    return (1. + np.sin(2.*pi*(p[0]*t+p[1]*x+p[2]*y)))/2.

@njit
def v_exact(t,x,y):
    return (1. + np.cos(2.*pi*(q[0]*t+q[1]*x+q[2]*y)))/2.


#--- Testing souce terms ---#
@njit
def source_phi(t,x,y):
    dtPhi = pi*p[0]*np.cos(2.*pi*(p[0]*t+p[1]*x+p[2]*y))
    ddPhi = -2.*pi**2*(p[1]**2 + p[2]**2)*np.sin(2.*pi*(p[0]*t+p[1]*x+p[2]*y))
    leftPartOfEqn = dtPhi - (par.D/par.L)*(par.tc/par.L)*ddPhi
    rightPartOfEqn = (par.alpha*par.tc)*(-f.dW(phi_exact(t,x,y)) + 0.5*f.dCm(phi_exact(t,x,y))*np.power(v_exact(t,x,y),2))
    return leftPartOfEqn - rightPartOfEqn

@njit
def source_v(t,x,y):
    dtV = -pi*q[0]*np.sin(2.*pi*(q[0]*t+q[1]*x+q[2]*y))
    lambdaV = l1*(v_exact(t,x,y)-0.5) + l0*0.5
    leftPartOfEqn = f.Cm(phi_exact(t,x,y))*dtV + par.tc*(f.Sm(phi_exact(t,x,y))*v_exact(t,x,y) + lambdaV)
    return leftPartOfEqn

#-==== ODE Solver ====-#
@njit
def odeF(t,x,y,psi,ow,ol,alpha,a1,a2):# psi = (phi,V)
    phi = psi[0]
    v = psi[1]
    fphi = (par.tc*alpha)*(-f.dW(phi,a1,a2) + 0.5*f.dCm(phi)*np.power(v,2))
    fv   = -par.tc*f.Sm(phi,ow,ol)*v/f.Cm(phi)
    #-- source terms associated to the numerical test
    fphi += source_phi(t,x,y)
    fv   += source_v(t,x,y)/f.Cm(phi)
    return np.stack((fphi,fv)) # np.array([fphi, fv])

@njit
def ode_timeStep(dt,t,x,y,psin,ow,ol,alpha,a1,a2): #Heun's method:
    psi_2 = psin + dt*odeF(t,x,y,psin,ow,ol,alpha,a1,a2)
    return psin + 0.5*dt*(odeF(t,x,y,psin,ow,ol,alpha,a1,a2)+odeF(t+dt,x,y,psi_2,ow,ol,alpha,a1,a2))

#-==== PDE Solver ====-#
# As we add all the source terms on the ODE split, this PDE scheme can be re-used from the main code. 

#-==== Testing Solver ====-#
@njit
def numerical_scheme(dt,t,x,y,psin,D=par.D,oe=par.oe,oc=par.oc,ow=par.ow,ol=par.ol,E=par.E,alpha=par.alpha,a1=par.a1,a2=par.a2,showInfo=False):
    psi_star      = ode_timeStep(dt/2.,t,x,y,psin,ow,ol,alpha,a1,a2)
    psi_star_star = pde.pde_timeStep(dt,t,psi_star,D,oe,oc,showInfo)
    psi           = ode_timeStep(dt/2.,t+dt/2.,x,y,psi_star_star,ow,ol,alpha,a1,a2)
    return psi
