import harmonics as h
import parameters as p
import numpy as np
import functions as f
import pyshtools as pysh
import harmonics as h
import functions as f
import numpy as np
import parameters as p
from numpy.linalg import norm
import pyshtools as pysh
import functions as f


#---------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------#
#-===== instance of unbounded operators ======-#

M = h.Id_sh + (0.5*p.dt/f.Cm(0.))*h.tc_DtN_sh #SHCoeff

def CGSolver(A, b, x0, eps=p.tol_CG, maxIter=p.maxIteration_CG, showIter=False, grid=p.grid):#SHGrid
    #A  : SHGrid -> SHGrid
    #b  : SHGrid
    #x0 : SHGrid (initial guess for iterative solver)
    #eps: convergence threshold
    x = x0
    #print("x0 =\n\n", x.to_array())
    r = b - A(x)
    #print("r =\n\n", r.to_array())
    z = (r.expand()/M).expand(grid=grid) # preconditioner
    #print("z =\n\n", z.to_array())
    p = z
    iter = 0
    error =  norm(r.to_array())
    #print("error0 = ",error)
    while error > eps and iter <= maxIter:
        #print("iteration = ",iter, " error = ",error)
        rho = np.vdot(r.to_array(),z.to_array())
        q = A(p)
        alpha = rho/np.vdot(p.to_array(),q.to_array())
        x = x + alpha*p
        r = r - alpha*q
        error = norm(r.to_array())
        z = (r.expand()/M).expand(grid=grid) # preconditioner
        beta = np.vdot(r.to_array(),z.to_array())/rho
        p = r + beta*p
        iter = iter + 1
    if iter > maxIter :
        print("max Iteration attained (", maxIter, ")!", "error =", error)
        time_file = open(o.simuFolder+"/ALERT.txt", "w")
        time_file.write("max Iteration attained (", maxIter, ")!", "error =", error)
        time_file.close()
    if showIter:
        print("CG-iteration =", iter, ". error = ", error)
    return x

#---------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------#

#----- PDE SPLIT FOR TMV -----#
# left-hand side of linear system AX=B (associated to a part of the pde splitting)
def lhs(dt,phi_star,X):#ShGrid
    #dt : float
    #phi_star : numpy array
    #X : ShGrid
    fs = pysh.SHGrid.from_array(np.sqrt(f.Cm(phi_star)), grid=p.grid)
    X0 = (h.tc_DtN_sh*((X/fs).expand())).expand(grid=p.grid)
    return (X + 0.5*dt*X0/fs)

def rhs(dt,vn,phin,phi_star):#ShGrid
    #dt : float
    #vn : SHGrid
    #phin : numpy array
    #phi_star : numpy array
    fn = pysh.SHGrid.from_array(f.Cm(phin), grid=p.grid)
    fs = pysh.SHGrid.from_array(np.sqrt(f.Cm(phi_star)), grid=p.grid)
    B = (h.tc_DtN_sh*vn.expand()).expand(grid=p.grid)
    return fs*(vn - 0.5*dt*B/fn)

def v_pde_timeStep(dt,phin,vn,phi_star,x0):#SHGrid
    #dt : float
    #phin : numpy_array
    #vn : SHGrid
    #phi_star : numpy_array
    B = rhs(dt,vn,phin,phi_star)
    A = partial(lhs,dt,phi_star)
    Y = cg.CGSolver(A, B, x0, showIter=p.showInfo)
    fs = pysh.SHGrid.from_array(np.sqrt(f.Cm(phi_star)), grid=p.grid)
    return Y/fs

#------ PDE SPLIT FOR PHI --------#
def phi_pde_timeStep(phin):#SHGrid
    #dt : float
    #phin : SHGrid
    return (h.Heat_sh*phin.expand()).expand(grid=p.grid)

##------ PDE SPLIT --------# STILL HAS TO BE CHECKED !!!!
def pde_timeStep(dt,psin,cg_x0=h.Null_grid):# np.array([SHGrid,SHGrid])
    #psin : np.array([phin,vn])
    #cg_x0 : SHGrid
    phis = phi_pde_timeStep(psin[0])
    vs   = v_pde_timeStep(dt,psin[0].to_array(),psin[1],phis.to_array(),cg_x0)
    return np.array([phis,vs])
