import matplotlib.pyplot as plt
import parameters as p
import organize as o
import harmonics as h
import pyshtools as pysh
import plotting as myplt
import pde
import ode
import time
import noise
import numpy as np
                    # ====== create folders & copy files ======= #
o.organize()
                            #====== INITIALIZATION ======#
seed  = 633137408
noise = noise.smooth_noise(seed=seed, amplitude=p.noiseAmp)
phi   = h.Null_grid + noise
tmv   = h.Null_grid
#tmv_cg_x0 = tmv # initial condition for CG_Solver
psi = np.array([phi,tmv])
                            #====== MEASURE TIME ======#
cpu_time = time.time_ns()
                            #======= SIMULATION ========#
t = 0. # [tchar]
savedFrames = 0
saveTimer = p.saveFreq
#saving initial condition
np.save(o.resultFolder+"/phi_"+str(savedFrames), psi[0].to_array())
np.save(o.resultFolder+"/v_"+str(savedFrames), psi[1].to_array())
#plot initial conditions
#fig, ax = plt.subplots(1,2)
#psi[0].plot(cmap='RdBu', cmap_reverse=True,show=False, ax=ax[0], colorbar='bottom')
#psi[1].plot(cmap='RdBu', cmap_reverse=True,show=False, ax=ax[1], colorbar='bottom')
#plt.show()

while (t < p.tf):
    if p.showInfo:
        print("------------------------")
        print("t = ", t*p.tc*1e9, " [ns]")
        print("saveFreq = ",p.saveFreq*p.tc*1e9, " [ns]")
    #-===== Strang Splitting =====-#
    psi = ode.ode_timeStep(p.dt/2., t, psi)
    psi = pde.pde_timeStep(p.dt, psi,h.Null_grid)#tmv_cg_x0)#
    psi = ode.ode_timeStep(p.dt/2., t+p.dt/2., psi)
    # ======= Updating ====== #
    t += p.dt
    #tmv_cg_x0 = psi[1]
    # ======== Saving ======= #
    if (t > saveTimer):
        np.save(o.resultFolder+"/phi_"+str(savedFrames), psi[0].to_array())# save phi
        np.save(o.resultFolder+"/v_"+str(savedFrames), psi[1].to_array())# save tmv
        savedFrames += 1
        saveTimer += p.saveFreq



#===== MEASURE TIME ======#
cpu_time = 1e-9*(time.time_ns() - cpu_time)/3600 #[Hours]
if p.showInfo:
    print("cpu_time = ", cpu_time ," Hours")
time_file = open(o.simuFolder+"/cpu_time.txt", "w")
time_file.write("Temps de calcul:\n"+"cpu_time= "+str(cpu_time)+" Hours\n")
time_file.write("Temps de calcul:\n"+"cpu_time= "+str(60.*cpu_time)+" Minutes\n")
time_file.close()
                    #=======- Plotting the solution -=======#
myplt.plotSolution(o.resultFolder,flatPlot=p.flatPlot, save=p.saveAnimation,show=p.showAnimation)
