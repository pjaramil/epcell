import numpy as np
import pyshtools as pysh
import parameters as par

#-======== Diagonalized Adimensional form of operators on the UNIT SPHERE ========-#
# Laplace-Beltrami operator
def laplaceEigens(lmax = par.L):
    laplace_SH = pysh.SHCoeffs.from_zeros(lmax,kind='real')
    for l in range(lmax+1):
        laplace_SH.set_coeffs(values=l*(l+1.), ls=l, ms=list(range(-l,l+1)))
    return laplace_SH #divide by Rc^2 for physical dimensions
# Main Steklov operator:
    #- helper function -#
def eigenVal(l,oe=par.oe,oc=par.oc,Re=par.Re,Rc=par.Rc):
    ql = (1. + l/(l+1.)*(Rc/Re)**(2*l+1))/(1. - (Rc/Re)**(2*l+1))
    return l*(l+1.)*ql/((oc/oe)*l + (l+1.)*ql)
    #- main operator definition
def lambdaEigens(lmax = par.L):
    DtN_SH = pysh.SHCoeffs.from_zeros(lmax, kind='real')
    for l in range(lmax+1):
        DtN_SH.set_coeffs(values=eigenVal(l), ls=l, ms=list(range(-l,l+1)) )
    return DtN_SH #multiply by (oc/Rc) for physical dimensions

#-===== Eigenvalue objetcs ======-#
lapEigens = laplaceEigens()
lambdaEigens0 = lambdaEigens()
