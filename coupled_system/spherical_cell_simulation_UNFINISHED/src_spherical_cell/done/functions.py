#from numba import jit, njit
import parameters as par
import numpy as np

# ALL FUNCTIONS IN THIS MODULE ARE SUPPOSED TO BE EVALUATED AT EACH POINT

#-========== HELPER FUNCTIONS ==========-#
#@njit(fastmath=True)
def sigmoidf(phi,s,r):
    return 0.5*(1.+ np.tanh(s*(phi-r)))

#@njit(fastmath=True)
def dsigmoidf(phi,s,r):
    return  0.5*s*(1.- np.power(np.tanh(s*(phi-r)),2))

#-========== MODEL FUNCTIONS ==========-#
#Modeling options:
#linearMixtureModel = False
smCoeff = 10.
smCutOff = 0.5
smModelWeight = 1e-4
tuning1 = 13.
tuning2 = 12.
cutOff1 = 1.

#@njit(fastmath=True)
def Cm(phi):
    delta = (par.ew**(1./3.) - par.el**(1./3.))
    c0 = np.power(par.el**(1./3.) + phi*delta,3) # Looyenga
    return (par.e0/par.h)*(c0*sigmoidf(phi,-tuning1,cutOff1) + par.ew*sigmoidf(phi,tuning2,cutOff1))

#@njit(fastmath=True)
def dCm(phi):
    delta = (par.ew**(1./3.) - par.el**(1./3.))
    c0 = np.power(par.el**(1./3.) + phi*delta,3)# Looyenga
    dc0 = 3.*delta*np.power(par.el**(1./3.) + phi*delta,2) # dLooyenga/dphi
    return (par.e0/par.h)*(dc0*sigmoidf(phi,-tuning1,1.) + c0*dsigmoidf(phi,-tuning1,1.) + par.ew*dsigmoidf(phi,tuning2,1.))

#@njit(fastmath=True)
def Sm(phi,ow=par.ow,ol=par.ol):
    return ((1. - smModelWeight)*sigmoidf(phi,smCoeff,smCutOff)/par.h + smModelWeight/par.h*phi)*(ow-ol) + ol/par.h

#@njit
def W(phi,a1=par.a1,a2=par.a2):
    return a1*np.power(phi,2)*np.power(phi - 1.,2) + a2*(phi + 0.5)*np.power(phi - 1.,2)

#@njit
def dW(phi,a1=par.a1,a2=par.a2):
    return 4.*a1*phi*(phi-1.)*(phi - 0.5 + 3./4.*(a2/a1))
