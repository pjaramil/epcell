#-- standard libraries
import numpy as np
#from numba import jit, njit
#-- my code
import parameters as par
import functions as f

#-==========- FUNCTIONS -==========-#
#@njit
def odeF(t,psi,ow,ol,oe,oc,E,alpha,a1,a2):# psi = (phi,V)
    phi = psi[0]
    v = psi[1]
    fphi = (par.tc*alpha)*(-f.dW(phi,a1,a2) + 0.5*f.dCm(phi)*np.power(v,2))
    fv   = -par.tc*f.Sm(phi,ow,ol)*v/f.Cm(phi)
    #fv   += par.tc*f.Gv(t,oe,oc,E)/f.Cm(phi)# Adding the source term
    return np.stack((fphi,fv))# np.array([fphi, fv])

#Heun's method:
#@njit
def ode_timeStep(dt,t,psin,ow,ol,oe,oc,E,alpha,a1,a2):
    psi_2 = psin + dt*odeF(t,psin,ow,ol,oe,oc,E,alpha,a1,a2)
    return psin + 0.5*dt*(odeF(t,psin,ow,ol,oe,oc,E,alpha,a1,a2) + odeF(t+dt,psi_2,ow,ol,oe,oc,E,alpha,a1,a2))
