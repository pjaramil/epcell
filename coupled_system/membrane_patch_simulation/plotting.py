import functions as f
import parameters as par
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as ani
from matplotlib import cm # hsv color map and Blues

def plot_CmSm_models(fPath):
    #Plotting the functions used in our model (Cm,Sm)
    phi = np.linspace(0.,1.,200)
    fig, ax = plt.subplots(2,1)
    # Cm plot
    ax[0].set_title(r'$Cm(\phi)$')
    ax[0].set_xlabel(r'$\phi$')
    ax[0].set_ylabel(r'$[F.m^{-2}]$')
    ax[0].grid()
    #ax[0].set_yscale('log')
    ax[0].plot(phi,f.Cm(phi))
    # Sm plot
    ax[1].set_title(r'$Sm(\phi)$')
    ax[1].set_xlabel(r'$\phi$')
    ax[1].set_ylabel(r'$[S.m^{-2}]$')
    ax[1].set_yscale('log')
    ax[1].grid()
    ax[1].plot(phi,f.Sm(phi))
    plt.tight_layout()
    fig.savefig(fPath+"/electric_properties_modeling.pdf")
    plt.close(fig)

def plot_graph(mean_vec,time_vec,savePath,title,xlabel,ylabel):
    fig, ax = plt.subplots()
    ax.set_title(title, fontsize='xx-large')
    ax.set_xlabel(xlabel, fontsize='x-large')
    ax.set_ylabel(ylabel, fontsize='x-large')
    ax.grid()
    ax.plot(time_vec,mean_vec)
    plt.tight_layout()
    fig.savefig(savePath)
    plt.close(fig)

def animateFlat(Zs,time_vec=[],title=None,barlabel=None,color="RdBu_r",show=False,save=True,fPath=None):
    #==== Setting up graph ====#
    fig, ax = plt.subplots()# figsize=(10.,6.)
    ax.set_title(title)
    ax.set_xlabel(r'[nm]')
    ax.set_ylabel(r'[nm]')
    #--- adding timer ---#
    if len(time_vec) > 0:
        timer = fig.add_axes([0.45, 0.95, 0.1, 0.025])
        timer.set_frame_on(False)
        timer.set_xticks([])
        timer.set_yticks([])
    #--- colorbar setup ---#
    Zs_max = np.max(Zs)
    Zs_min = np.min(Zs)
    tickList = np.linspace(Zs_min,Zs_max,3).tolist()
    #--- Setting grid points ---#
    tx = (par.L*1e9)*np.linspace(0.,1.,par.nx,endpoint=False)
    ty = (par.L*1e9)*np.linspace(0.,1.,par.ny,endpoint=False)
    X , Y = np.meshgrid(tx,ty)
    #===== First frame =====#
    im = ax.pcolormesh(X,Y,Zs[0],vmin=Zs_min,vmax=Zs_max,cmap=color)
    fig.colorbar(im,ax=ax,location="right",shrink=1.,ticks=tickList,format="{x:.2e}",label=barlabel,aspect=40,orientation="vertical")
    if len(time_vec) > 0:
        timerTik = timer.text(0.25,0.25,"time  = "+ str(round(time_vec[0])) + " [ns]")
        frame = [im,timerTik]
    else:
        frame = [im]
    artists = [frame]
    #===== rest of frames =====#
    for i in range(1, np.shape(Zs)[0]):
        im = ax.pcolormesh(X,Y,Zs[i],vmin=Zs_min,vmax=Zs_max,cmap=color)
        if len(time_vec) > 0:
            timerTik = timer.text(0.25,0.25,"time  = "+ str(round(time_vec[i])) + " [ns]")
            frame = [im,timerTik]
        else:
            frame = [im]
        artists.append(frame)
    #===== animate =====#
    animation = ani.ArtistAnimation(fig=fig, artists=artists, interval=200)
    #===== save/show =====#
    if save:
        if fPath == None: # Assuming we are running this function from the result folder
            animation.save(filename="../graphs/localFlatAnimation.gif",writer="pillow")
        else:
            animation.save(filename=fPath,writer="pillow")
    if show:
        plt.show()

def animate3D(Zs,time_vec=[],title=None,barlabel=None,color="RdBu_r",show=False,save=True,fPath=None):
    #==== Setting up graph ====#
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    ax.set_title(title)
    ax.set_xlabel(r'[nm]')
    ax.set_ylabel(r'[nm]')
    #--- adding timer ---#
    if len(time_vec) > 0:
        timer = fig.add_axes([0.45, 0.95, 0.1, 0.025])
        timer.set_frame_on(False)
        timer.set_xticks([])
        timer.set_yticks([])
    #--- normalize colorbar ---#
    Zs_max = np.max(Zs)
    Zs_min = np.min(Zs)
    tickList = np.linspace(Zs_min,Zs_max,3).tolist()
    #--- Setting grid points ---#
    tx = (par.L*1e9)*np.linspace(0.,1.,par.nx,endpoint=False)
    ty = (par.L*1e9)*np.linspace(0.,1.,par.ny,endpoint=False)
    X , Y = np.meshgrid(tx,ty)
    #===== First frame =====#
    im = ax.plot_surface(X,Y,Zs[0],vmin=Zs_min,vmax=Zs_max,cmap=color)
    fig.colorbar(im,ax=ax,location=None,shrink=0.5,ticks=tickList,format="{x:.2e}",label=barlabel,aspect=10,orientation="horizontal")
    if len(time_vec) > 0:
        timerTik = timer.text(0.25,0.25,"time  = "+ str(round(time_vec[0])) + " [ns]")
        frame = [im,timerTik]
    else:
        frame = [im]
    artists = [frame]
    #===== rest of frames =====#
    for i in range(1, np.shape(Zs)[0]):
        im = ax.plot_surface(X,Y,Zs[i],vmin=Zs_min,vmax=Zs_max,cmap=color)
        if len(time_vec) > 0:
            timerTik = timer.text(0.25,0.25,"time  = "+ str(round(time_vec[i])) + " [ns]")
            frame = [im,timerTik]
        else:
            frame = [im]
        artists.append(frame)
    #===== animate =====#
    animation = ani.ArtistAnimation(fig=fig, artists=artists, interval=200)
    #===== save/show =====#
    if save:
        if fPath == None: # Assuming we are running this function from the result folder
            animation.save(filename="../graphs/local3DAnimation.gif",writer="pillow")
        else:
            animation.save(filename=fPath,writer="pillow")
    if show:
        plt.show()

