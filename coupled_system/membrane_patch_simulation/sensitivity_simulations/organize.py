import os
import shutil
import glob

def setup_folders(filePath=None):
    if filePath == None:
        simuFolder = "../results"
    else:
        simuFolder = filePath
    try:
        os.mkdir(simuFolder)
    except OSError as error:
        print(error)
        print("Replacing this folder with a newer version.")
        shutil.rmtree(simuFolder)
        os.mkdir(simuFolder)
    finally:
        os.mkdir(simuFolder + "/data")
        os.mkdir(simuFolder + "/code")
        os.mkdir(simuFolder + "/graphs")
        for file in glob.glob("*.py"):
            shutil.copy(file, simuFolder+"/code")

