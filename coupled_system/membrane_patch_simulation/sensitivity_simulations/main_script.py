#-- std libraries
import numpy as np
import time
import glob
from numpy.linalg import norm
#-- my files
import splittingScheme as splitting
import parameters as par
import organize as org
import functions as f
import initialCondition as ic
import plotting as plt

#-===== Simulation Parameters =====-#
showInfo = False
surfacePlot = True
flatPlot = True
mean_savePeriod = (2e-9/par.tc) # [tc]
solution_savePeriod = (1e-7/par.tc) # [tc]
dt = 1./par.nt# [tchar]

#======= Testing parameters =======#
#changes here have to be applied to the inner loop as well
m_range = 5
range_vec = np.linspace(0.5,2.5,m_range)
paramStr = "a_1"
paramBaseValue = par.alpha
paramLegend = r'$a_{1}$'

#======= create folders & copy files =======#
cpu_time = time.time_ns()
fname = str(cpu_time)[3:-6] + "_sensitivityTest_" + paramStr
folder = "/home/pedrojaramillo/Documents/Code/work/numericalResults"
folder += "/" + fname
org.setup_folders(filePath=folder)

#-===== Set of simulations =====-#
mean_phis = []
mean_vs = []
for r in range_vec:
    #======= INITIALIZATION =======#
    t = 0. # [tc]
    phin = ic.rand_smooth_phi(par.nx,par.ny,pmax=par.noise_magnitude)
    vn = np.zeros((par.nx,par.ny))
    psin = np.stack((phin,vn))
    #----------- mean initial condition -----------#
    mean_timer = [0.]
    solution_timer = [0.]
    mean_phi = [f.mean(psin[0])]
    mean_v = [f.mean(psin[1])]
    #===================== SIMULATION ====================#
    for i in range(int(par.nt*par.tf)):
        if showInfo:
            print("(r,t) = ", (r,t*par.tc*1e9), "([1],[ns])")
        #------ Strang Splitting ------#
        psin = splitting.numerical_scheme(t,dt,psin,a1=r*par.a1,showInfo=showInfo)
        #------ Updating ------#
        t = t + dt
        #------ Saving/Accumulating ------#
        if (t > mean_timer[-1] + mean_savePeriod):
            mean_v.append(f.mean(psin[1]))
            mean_phi.append(f.mean(psin[0]))
            mean_timer.append(mean_timer[-1] + mean_savePeriod)
        if (t > solution_timer[-1] + solution_savePeriod):
            strNum = "_{:0>5}".format(len(solution_timer))
            strNum += "_r_" + "{0:.2f}".format(r)
            np.save(folder+"/data/phi"+strNum, psin[0])
            np.save(folder+"/data/v"+strNum, psin[1])
            solution_timer.append(solution_timer[-1] + solution_savePeriod)
    #================= Saving =================#
    #---- from list to arrays ----#
    mean_phi_vec = np.array(mean_phi)
    mean_v_vec = np.array(mean_v)
    mean_timer_vec = np.array(mean_timer)*par.tc*1e9# [ns]
    solution_timer_vec = np.array(solution_timer)*par.tc*1e9# [ns]
    #---- Saving ----#
    np.save(folder+"/data/mean_timer_"+paramStr+"_r_{0:.2f}".format(r), mean_timer_vec)
    np.save(folder+"/data/mean_phi_"+paramStr+"_r_{0:.2f}".format(r), mean_phi_vec)
    np.save(folder+"/data/mean_v_"+paramStr+"_r_{0:.2f}".format(r), mean_v_vec)
    #---- appending ----#
    mean_phis.append((r, paramBaseValue, mean_phi))
    mean_vs.append((r, paramBaseValue, mean_v))

#================= Plotting =================#
#----------------- mean behavior -----------------#
mean_phis = sorted(mean_phis,key=lambda y: y[0])
mean_vs = sorted(mean_vs,key=lambda y: y[0])
phiGraphPath = folder+'/graphs/phi_'+paramStr+'_sensitivityTest.pdf'
vGraphPath = folder+'/graphs/v_'+paramStr+'_sensitivityTest.pdf'
plt.batch_plot_means(mean_timer_vec,mean_phis,phiGraphPath,paramLegend,r"$t~[ns]$",r"[1]")
plt.batch_plot_means(mean_timer_vec,mean_vs,vGraphPath,paramLegend,r"$t~[ns]$",r"[V]")
#----------------- solution -----------------#
for r in range_vec:
    #--- Getting the data ---#
    rNum = "_r_" + "{0:.2f}".format(r)
    phi_files = glob.glob(folder+"/data/phi_*"+rNum+".npy")
    v_files = glob.glob(folder+"/data/v_*"+rNum+".npy")
    #--- arranging ---#
    phi_files.sort()
    v_files.sort()
    #--- to arrays ---#
    Zps = []
    Zvs = []
    for file in phi_files:
        Zps.append(np.load(file))
    for file in v_files:
        Zvs.append(np.load(file))
    Zps = np.array(Zps).astype(float)
    Zvs = np.array(Zvs).astype(float)
    #--- solution plotting ---#
    if flatPlot:
        fname_phi = folder+"/graphs/phi_flat"+rNum+".gif"
        fname_v = folder+"/graphs/v_flat"+rNum+".gif"
        plt.animateFlat(Zps,solution_timer_vec, title=r'$\phi$',barlabel='[1]',fPath=fname_phi)
        plt.animateFlat(Zvs,solution_timer_vec, title=r'TMV',barlabel='[1]',fPath=fname_v,color='cool')
    if surfacePlot:
        sname_phi = folder+"/graphs/phi_3D"+rNum+".gif"
        sname_v = folder+"/graphs/v_3D"+rNum+".gif"
        plt.animate3D(Zps,solution_timer_vec, title=r'$\phi$',barlabel='[1]',fPath=sname_phi)
        plt.animate3D(Zvs,solution_timer_vec, title=r'TMV',barlabel='[1]',fPath=sname_v,color='cool_r')
