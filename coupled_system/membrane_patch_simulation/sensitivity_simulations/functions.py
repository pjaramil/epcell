import parameters as par
import numpy as np
from math import pi
from numpy.fft import fft2, ifft2
from numba import jit, njit

#-========== HELPER FUNCTIONS ==========-#
@njit(fastmath=True)
def mean(phi):
    nx,ny = np.shape(phi)
    return np.real(fft2(phi)[0,0])/nx/ny

@njit(fastmath=True)
def sigmoidf(phi,s,r):
    return 0.5*(1.+ np.tanh(s*(phi-r)))

@njit(fastmath=True)
def dsigmoidf(phi,s,r):
    return  0.5*s*(1.- np.power(np.tanh(s*(phi-r)), 2))

#-========== FUNCTIONS ==========-#
@njit
def G(t,oe=par.oe,oc=par.oc,E=par.E):
    return 2.*oe*oc/(oe+oc)*E*(t<=par.tPulse)

#-==== Modeling options =====-#
linearMixtureModel = False
smCoeff = 10.
smCutOff = 0.5
smModelWeight = 1e-4
tuning1 = 13.
tuning2 = 12.
cutOff1 = 1.

@njit(fastmath=True)
def Cm(phi):
    if linearMixtureModel:
        delta = par.ew - par.el
        c0 = par.el + phi*delta # Linear
    else:
        delta = (par.ew**(1./3.) - par.el**(1./3.))
        c0 = np.power(par.el**(1./3.) + phi*delta, 3) # Looyenga
    return (par.e0/par.h)*(c0*sigmoidf(phi,-tuning1,cutOff1) + par.ew*sigmoidf(phi,tuning2,cutOff1))

@njit(fastmath=True)
def dCm(phi):
    if linearMixtureModel:
        delta = par.ew - par.el
        c0 = par.el + phi*delta
        dc0 = delta
    else:
        delta = (par.ew**(1./3.) - par.el**(1./3.))
        c0 = np.power(par.el**(1./3.) + phi*delta, 3)# Looyenga
        dc0 = 3.*delta*np.power(par.el**(1./3.) + phi*delta, 2) # dLooyenga/dphi
    return (par.e0/par.h)*(dc0*sigmoidf(phi,-tuning1,1.) + c0*dsigmoidf(phi,-tuning1,1.) + par.ew*dsigmoidf(phi,tuning2,1.))

@njit(fastmath=True)
def Sm(phi,ow=par.ow,ol=par.ol):
    return ((1. - smModelWeight)*sigmoidf(phi,smCoeff,smCutOff)/par.h + smModelWeight/par.h*phi)*(ow-ol) + ol/par.h

@njit
def W(phi,a1=par.a1,a2=par.a2):
    return a1*np.power(phi, 2)*np.power(phi-1., 2) + a2*(phi + 0.5)*np.power(phi-1., 2)

@njit
def dW(phi,a1=par.a1,a2=par.a2):
    return 4.*a1*phi*(phi-1.)*(phi - 0.5 + 3./4.*(a2/a1))

