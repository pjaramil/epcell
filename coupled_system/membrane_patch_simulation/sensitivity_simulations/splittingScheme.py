#-- std libraries
import numpy as np
from numba import jit, njit
#-- my files
import parameters as par
import ODE_timeStep as ode
import PDE_timeStep as pde

@njit
def numerical_scheme(t,dt,psin,D=par.D,oe=par.oe,oc=par.oc,ow=par.ow,ol=par.ol,E=par.E,alpha=par.alpha,a1=par.a1,a2=par.a2,showInfo=False):
    psi_star      = ode.ode_timeStep(dt/2.,t,psin,oe,oc,ow,ol,E,alpha,a1,a2)
    psi_star_star = pde.pde_timeStep(dt,t,psi_star,D,oe,oc,showInfo)
    psi           = ode.ode_timeStep(dt/2.,t+dt/2.,psi_star_star,oe,oc,ow,ol,E,alpha,a1,a2)
    return psi

