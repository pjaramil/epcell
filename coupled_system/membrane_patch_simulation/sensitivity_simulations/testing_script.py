#-- std libraries
from numpy.linalg import norm
import numpy as np
import time
import glob
#-- my files
import parameters as par
import functions as f
import testing as test
import plotting as plt

#-===== Simulation Parameters =====-#
showInfo = True
showAnimation = False
saveAnimation = True
flatPlot = True
surfacePlot = True
solution_savePeriod = (2.5e-8/par.tc) # [tc]
mean_savePeriod = (2e-9/par.tc) # [tc]
dt = 1./par.nt# [tchar]
t = 0. # [tc]

#======= INITIALIZATION =======#
#-- Space variables --#
x = np.linspace(0.,1.,par.nx,endpoint=False)
y = np.linspace(0.,1.,par.ny, endpoint=False)
X , Y = np.meshgrid(x,y)
#-- Testing ---#
phin = test.phi_exact(0.,X,Y)
vn = test.v_exact(0.,X,Y)
psin = np.stack((phin,vn))
#-- measures of the error --#
phi_error = 0.*phin
v_error = 0.*vn

#=====================================================#
#===================== SIMULATION ====================#
#while (t < par.tf):
for i in range(int(par.nt*par.tf)):
    if showInfo:
        print("------------------------")
        print("t = ", t*par.tc*1e9, " [ns]")
        #print("L = ", par.L*1e9, " [nm]")
        print("||phi - phi_exact|| =", norm(phi_error/par.nx, ord="fro"))
        print("||v - v_exact|| =", norm(v_error/par.nx, ord="fro"))
    #------ Strang Splitting ------#
    psin = test.numerical_scheme(dt,t,X,Y,psin,showInfo=True)
    #------ Updating ------#
    t += dt
    #----- Measuring error ------#
    phi_error = psin[0] - test.phi_exact(t,X,Y)
    v_error = psin[1] - test.v_exact(t,X,Y)
#================= END OF SIMULATION =================#
#=====================================================#
