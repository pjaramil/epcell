import numpy as np
import unboundedOperators as u
from numpy.fft import fft2, ifft2
from math import pi
from numba import jit, njit

# Data
seed = 633137408
sigma = 0.005# 2*sigma ~ the noise amplitude.
mu = 0.
D0 = 7.5e-4

# functions
def heatFlow(dt,nx,ny,phin):# periodic heatflow
    return ifft2(np.exp(-dt*D0*u.laplaceEigens(Nx=nx,Ny=ny))*fft2(phin))

def rand_smooth_phi(nx,ny,pmax=1.):
    phin = np.random.default_rng(seed).normal(mu,sigma,(nx,ny))
    phin = heatFlow(1.,nx,ny,phin)
    phin = phin - phin.min()
    return pmax*phin/phin.max()

def explicit_smooth_hole_phi(nx,ny,r,pmax=1.):
    X , Y = np.meshgrid(np.linspace(0.,1.,nx,endpoint=False),np.linspace(0.,1.,ny, endpoint=False))
    phin = np.zeros((nx,ny))
    phin += 1. - np.tanh(10.*(np.sin(pi*(X-0.5))**2 + np.sin(pi*(Y-0.5))**2)**2)
    return phin/np.max(phin)

def explicit_four_smooth_hole_phi(nx,ny,r,pmax=1.):
    X , Y = np.meshgrid(np.linspace(0.,1.,nx,endpoint=False),np.linspace(0.,1.,ny, endpoint=False))
    phin = np.zeros((nx,ny))
    phin += 1. - np.tanh(10.*(np.sin(pi*(2.*X-0.5))**2 + np.sin(pi*(2.*Y-0.5))**2)**2)
    return phin/np.max(phin)
