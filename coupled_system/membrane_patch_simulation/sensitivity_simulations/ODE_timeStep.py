#-- std libraries
import numpy as np
from numba import jit, njit
#-- my files
import parameters as par
import unboundedOperators as u
import functions as f

#-==========- FUNCTIONS -==========-#
@njit
def odeF(t,psi,oe,oc,ow,ol,E,alpha,a1,a2):# psi = (phi,V)
    phi = psi[0]
    v = psi[1]
    fphi = (par.tc*alpha)*(-f.dW(phi,a1,a2) + 0.5*f.dCm(phi)*np.power(v,2))
    fv   = -par.tc*f.Sm(phi,ow,ol)*v/f.Cm(phi)
    fv   += par.tc*f.G(t,oe,oc,E)/f.Cm(phi)
    return np.stack((fphi,fv))#np.array([fphi, fv])

@njit
def ode_timeStep(dt,t,psin,oe,oc,ow,ol,E,alpha,a1,a2): #Heun's method:
    psi_2 = psin + dt*odeF(t,psin,oe,oc,ow,ol,E,alpha,a1,a2)
    return psin + 0.5*dt*(odeF(t,psin,oe,oc,ow,ol,E,alpha,a1,a2)+odeF(t+dt,psi_2,oe,oc,ow,ol,E,alpha,a1,a2))
