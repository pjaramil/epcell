import functions as f
import parameters as par
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as ani
from matplotlib import cm # hsv color map and Blues

def batch_plot_means(time_vec,mean_vecs,savePath,paramLegend,xlabel,ylabel):
    fig, ax = plt.subplots()
    ax.set_title(paramLegend+'-sensitivity test', fontsize='xx-large')
    ax.set_xlabel(xlabel, fontsize='x-large')
    ax.set_ylabel(ylabel, fontsize='x-large')
    ax.grid()
    for (r,baseValue,vec) in mean_vecs:
        if r == 1.:
            ax.plot(time_vec,vec,label=paramLegend+'={:.2e}'.format(r*baseValue),color='black')
        else:
            ax.plot(time_vec,vec,label=paramLegend+'={:.2e}'.format(r*baseValue))
    ax.legend()
    plt.tight_layout()
    fig.savefig(savePath)
    plt.close(fig)

def plot_graph(mean_vec,time_vec,savePath,title,xlabel,ylabel):
    fig, ax = plt.subplots()
    ax.set_title(title, fontsize='xx-large')
    ax.set_xlabel(xlabel, fontsize='x-large')
    ax.set_ylabel(ylabel, fontsize='x-large')
    ax.grid()
    ax.plot(time_vec,mean_vec)
    plt.tight_layout()
    fig.savefig(savePath)
    plt.close(fig)

def animateFlat(Zs,time_vec=[],title=None,barlabel=None,color="RdBu_r",show=False,save=True,fPath=None):
    #==== Setting up graph ====#
    fig, ax = plt.subplots()# figsize=(10.,6.)
    ax.set_title(title)
    ax.set_xlabel(r'[nm]')
    ax.set_ylabel(r'[nm]')
    #--- adding timer ---#
    if len(time_vec) > 0:
        timer = fig.add_axes([0.45, 0.95, 0.1, 0.025])
        timer.set_frame_on(False)
        timer.set_xticks([])
        timer.set_yticks([])
    #--- normalize colorbar ---#
    Zs_max = np.max(Zs)
    Zs_min = np.min(Zs)
    #--- Setting grid points ---#
    tx = (par.L*1e9)*np.linspace(0.,1.,par.nx,endpoint=False)
    ty = (par.L*1e9)*np.linspace(0.,1.,par.ny,endpoint=False)
    X , Y = np.meshgrid(tx,ty)
    #===== First frame =====#
    im = ax.pcolormesh(X,Y,Zs[0],vmin=Zs_min,vmax=Zs_max,cmap=color)
    fig.colorbar(im,ax=ax,location="right",shrink=1.,label=barlabel,aspect=40,orientation="vertical")
    if len(time_vec) > 0:
        timerTik = timer.text(0.25,0.25,"time  = "+ str(round(time_vec[0])) + " [ns]")
        frame = [im,timerTik]
    else:
        frame = [im]
    artists = [frame]
    #===== rest of frames =====#
    for i in range(1, np.shape(Zs)[0]):
        im = ax.pcolormesh(X,Y,Zs[i],vmin=Zs_min,vmax=Zs_max,cmap=color)
        if len(time_vec) > 0:
            timerTik = timer.text(0.25,0.25,"time  = "+ str(round(time_vec[i])) + " [ns]")
            frame = [im,timerTik]
        else:
            frame = [im]
        artists.append(frame)
    #===== animate =====#
    animation = ani.ArtistAnimation(fig=fig, artists=artists, interval=200)
    #===== save/show =====#
    if save:
        if fPath == None: # Assuming we are running this function from the result folder
            animation.save(filename="../graphs/localFlatAnimation.gif",writer="pillow")
        else:
            animation.save(filename=fPath,writer="pillow")
    if show:
        plt.show()

def animate3D(Zs,time_vec=[],title=None,barlabel=None,color="RdBu_r",show=False,save=True,fPath=None):
    #==== Setting up graph ====#
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    ax.set_title(title)
    ax.set_xlabel(r'[nm]')
    ax.set_ylabel(r'[nm]')
    #--- adding timer ---#
    if len(time_vec) > 0:
        timer = fig.add_axes([0.45, 0.95, 0.1, 0.025])
        timer.set_frame_on(False)
        timer.set_xticks([])
        timer.set_yticks([])
    #--- normalize colorbar ---#
    Zs_max = np.max(Zs)
    Zs_min = np.min(Zs)
    #--- Setting grid points ---#
    tx = (par.L*1e9)*np.linspace(0.,1.,par.nx,endpoint=False)
    ty = (par.L*1e9)*np.linspace(0.,1.,par.ny,endpoint=False)
    X , Y = np.meshgrid(tx,ty)
    #===== First frame =====#
    im = ax.plot_surface(X,Y,Zs[0],vmin=Zs_min,vmax=Zs_max,cmap=color)
    fig.colorbar(im,ax=ax,location=None,shrink=0.5,label=barlabel,aspect=10,orientation="horizontal")
    if len(time_vec) > 0:
        timerTik = timer.text(0.25,0.25,"time  = "+ str(round(time_vec[0])) + " [ns]")
        frame = [im,timerTik]
    else:
        frame = [im]
    artists = [frame]
    #===== rest of frames =====#
    for i in range(1, np.shape(Zs)[0]):
        im = ax.plot_surface(X,Y,Zs[i],vmin=Zs_min,vmax=Zs_max,cmap=color)
        if len(time_vec) > 0:
            timerTik = timer.text(0.25,0.25,"time  = "+ str(round(time_vec[i])) + " [ns]")
            frame = [im,timerTik]
        else:
            frame = [im]
        artists.append(frame)
    #===== animate =====#
    animation = ani.ArtistAnimation(fig=fig, artists=artists, interval=200)
    #===== save/show =====#
    if save:
        if fPath == None: # Assuming we are running this function from the result folder
            animation.save(filename="../graphs/local3DAnimation.gif",writer="pillow")
        else:
            animation.save(filename=fPath,writer="pillow")
    if show:
        plt.show()
