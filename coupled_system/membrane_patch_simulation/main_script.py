#-- std libraries
from numpy.linalg import norm
import numpy as np
import time
import glob
#-- my files
import parameters as par
import organize as org
import functions as f
import initialCondition as ic
import splittingScheme as splitting
import testing as test
import plotting as plt

#-===== Simulation Parameters =====-#
showInfo = False
showAnimation = False
saveAnimation = True
flatPlot = True
surfacePlot = True
solution_savePeriod = (2.5e-8/par.tc) # [tc]
mean_savePeriod = (1e-9/par.tc) # [tc]
dt = 1./par.nt# [tchar]
t = 0. # [tc]

#======= create folders & copy files =======#
cpu_time = time.time_ns()
fname = str(cpu_time)[3:-6]
folder = "~/Documents/NumericalResults/"
folder +=  fname + "_membranePatch"
org.setup_folders(filePath=folder)

#======= INITIALIZATION =======#
phin = ic.rand_smooth_phi(par.nx,par.ny,pmax=par.noise_magnitude)
vn = np.zeros((par.nx,par.ny))
psin = np.stack((phin,vn))

#===== Saving initial conditions ======#
solution_timer = [0.]
mean_timer = [0.]
#----------------- solution -----------------#
np.save(folder+"/data/phi_{:0>5}".format(0), psin[0])
np.save(folder+"/data/v_{:0>5}".format(0), psin[1])
#----------------- mean behavior -----------------#
mean_phi = [f.mean(psin[0])]
mean_v = [f.mean(psin[1])]
mean_relative_sm = [f.mean(f.Sm(psin[0])/f.Sm(0.))]
mean_relative_cm = [f.mean(f.Cm(psin[0])/f.Cm(0.))]
mean_static_energy = [f.mean(0.5*f.Cm(psin[0])*np.power(psin[1],2))]

#=====================================================#
#===================== SIMULATION ====================#
#while (t < par.tf):
for i in range(int(par.nt*par.tf)):
    if showInfo:
        print("------------------------")
        print("t = ", t*par.tc*1e9, " [ns]")
        #print("L = ", par.L*1e9, " [nm]")
        #print("E = ", par.E, " [V/m]")
        print("||phi - phi_exact|| =", norm(phi_error/par.nx, ord="fro"))
        print("||v - v_exact|| =", norm(v_error/par.nx, ord="fro"))
    #------ Strang Splitting ------#
    psin = splitting.numerical_scheme(t,dt,psin,showInfo=showInfo)
    #------ Updating ------#
    t += dt
    #------ Saving/Accumulating ------#
    if (t > mean_timer[-1] + mean_savePeriod):
        mean_v.append(f.mean(psin[1]))
        mean_phi.append(f.mean(psin[0]))
        mean_relative_sm.append(f.mean(f.Sm(psin[0])/f.Sm(0.)))
        mean_relative_cm.append(f.mean(f.Cm(psin[0])/f.Cm(0.)))
        mean_static_energy.append(f.mean(0.5*f.Cm(psin[0])*np.power(psin[1],2)))
        mean_timer.append(mean_timer[-1]+mean_savePeriod)
    if (t > solution_timer[-1] + solution_savePeriod):
        np.save(folder+"/data/phi_{:0>5}".format(len(solution_timer)),psin[0])
        np.save(folder+"/data/v_{:0>5}".format(len(solution_timer)),psin[1])
        solution_timer.append(solution_timer[-1]+solution_savePeriod)
#================= END OF SIMULATION =================#
#=====================================================#

#================= Saving mean evolution =================#
#---- from list to arrays ----#
#- time -#
solution_timer = np.array(solution_timer)*par.tc*1e9 #[ns]
mean_timer = np.array(mean_timer)*par.tc*1e9 #[ns]
#- other -#
mean_static_energy = np.array(mean_static_energy)
mean_phi = np.array(mean_phi)
mean_v = np.array(mean_v)
mean_relative_sm = np.array(mean_relative_sm)
mean_relative_cm = np.array(mean_relative_cm)
#---- Saving ----#
np.save(folder+"/data/solution_timer", solution_timer)
np.save(folder+"/data/mean_timer", mean_timer)
np.save(folder+"/data/mean_static_energy", mean_static_energy)
np.save(folder+"/data/mean_v", mean_v)
np.save(folder+"/data/mean_phi", mean_phi)
np.save(folder+"/data/mean_relative_sm", mean_relative_sm)
np.save(folder+"/data/mean_relative_cm", mean_relative_cm)

#================= Plotting =================#
#---------- electric properties ----------#
plt.plot_CmSm_models(folder+"/graphs")
#----------------- solution -----------------#
#--- Getting the data ---#
phi_files = glob.glob(folder+"/data/phi_*.npy")
v_files = glob.glob(folder+"/data/v_*.npy")
#--- arranging ---#
phi_files.sort()
v_files.sort()
#--- to arrays ---#
Zps = []
Zvs = []
for file in phi_files:
    Zps.append(np.load(file))
for file in v_files:
    Zvs.append(np.load(file))
Zps = np.array(Zps).astype(float)
Zvs = np.array(Zvs).astype(float)
Zenergys = 0.5*f.Cm(Zps)*np.power(Zvs,2)

#--- solution plotting ---#
if flatPlot:
   plt.animateFlat(Zenergys,solution_timer, title=r'$\frac{1}{2}C_{m}(\phi)v^{2}$',barlabel=r'[$J.m^{-2}$]',fPath=folder+"/graphs/energy_flat.gif",show=showAnimation)
   plt.animateFlat(Zps,solution_timer, title=r'$\phi$',barlabel='[1]',fPath=folder+"/graphs/phi_flat.gif",show=showAnimation)
   plt.animateFlat(Zvs,solution_timer, title=r'TMV',barlabel='[V]',fPath=folder+"/graphs/v_flat.gif",color='cool',show=showAnimation)
if surfacePlot:
   plt.animate3D(Zenergys,solution_timer, title=r'$\frac{1}{2}C_{m}(\phi)v^{2}$',barlabel=r'[$J.m^{-2}$]',fPath=folder+"/graphs/energy_3D.gif",show=showAnimation)
   plt.animate3D(Zps,solution_timer, title=r'$\phi$',barlabel='[1]',fPath=folder+"/graphs/phi_3D.gif",show=showAnimation)
   plt.animate3D(Zvs,solution_timer, title=r'TMV',barlabel='[V]',fPath=folder+"/graphs/v_3D.gif",color='cool_r',show=showAnimation)

#--- mean behavior plotting ---#
plt.plot_graph(mean_static_energy,mean_timer,folder+"/graphs/mean_static_energy_density.pdf",r"$\frac{1}{|\mathbb{T}\,|}\int_{\mathbb{T}}\frac{1}{2}C_{m}(\phi)v^{2}\,dx$",r"$t~[ns]$",r"[$J.m^{-2}$]")
plt.plot_graph(mean_phi,mean_timer,folder+"/graphs/mean_phi.pdf",r"$\frac{1}{|\mathbb{T}\,|}\int_{\mathbb{T}}\phi\,dx$",r"$t~[ns]$",r"[1]")
plt.plot_graph(mean_v,mean_timer,folder+"/graphs/mean_v.pdf",r"$\frac{1}{|\mathbb{T}\,|}\int_{\mathbb{T}}V\,dx$",r"$t~[ns]$",r"[V]")
plt.plot_graph(mean_relative_sm,mean_timer,folder+"/graphs/mean_relative_Sm.pdf",r"$\frac{1}{|\mathbb{T}\,|}\int_{\mathbb{T}}\frac{S_{m}(\phi)}{S_{m}(0)}\,dx$",r"$t~[ns]$",r"[1]")
plt.plot_graph(mean_relative_cm,mean_timer,folder+"/graphs/mean_relative_Cm.pdf",r"$\frac{1}{|\mathbb{T}\,|}\int_{\mathbb{T}}\frac{C_{m}(\phi)}{C_{m}(0)}\,dx$",r"$t~[ns]$",r"[1]")
