# EPCell

## Source Code

- There are a total of 5 folders which consist of the following simulation code:
    1) Coupled system of equations on the setting of a periodic cell membrane patch
    2) Coupled system of equations on the setting of the spherical cell membrane
    3) Transmembrane voltage quasi-static model (decoupled from the membrane phase field) in periodic membrane patch
    4) Sensitivity analysis in the setting of a periodic membrane patch
    5) Linear stability analysis widget. Only the flat membrane patch setting works for now.

Contact: pedro.jaramillo-aguayo@inria.fr

[1] P. Jaramillo-Aguayo, A. Collin, and C. Poignard. Phase-field model of bilipid membrane electroporation. Journal of
Mathematical Biology, 87(1) :18, 2023.

[2] P. Jaramillo-Aguayo, A. Collin, and C. Poignard. Second order splitting scheme for a new phase-field electroporation model. Submitted, 2024.

## Example of simulations
![title](images/flat_bigPatch.png)
![title](images/smallPatch_1.png)
![title](images/smallPatch_2.png)
![title](images/smallPatch_3.png)

