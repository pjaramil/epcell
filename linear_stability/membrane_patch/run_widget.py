import model.parameters as p
import model.functions as f
import numpy as np
from numpy.linalg import det
import matplotlib.pyplot as plt
from matplotlib import cm # colormaps
#from scipy.fft import fft2, ifft2, fftfreq
from numpy.fft import fft2, ifft2, fftfreq
from math import pi
import matplotlib.lines as lines
from matplotlib.widgets import Button, Slider

eigenStart = 0
eigenEnd = 2000
eigenStep = 1
lapEigens = p.laplaceEigens(eigenStart,eigenEnd,eigenStep)
lambdaEigens = p.lambdaEigens(eigenStart,eigenEnd,eigenStep)
#--- Matrix entries
# A = [ a  b ]
#     [ c  d ]
#a = (D*tc/L**2)*lapEigens - tc*alpha*f.ddW(phiBar) + tc*alpha/2.*f.ddCm(phiBar)*vBar**2
#b = tc*alpha*f.dCm(phiBar)*vBar
#c = -tc*f.dSm(phiBar)*vBar/f.Cm(phiBar)
#d = -tc/f.Cm(phiBar)*(f.Sm(phiBar) + lambdaEigens)
def eigenPlus(a1,alpha,sigmoidCoeff,sigmoidCutOff,smModelWeight,ol,D,phiBar):
    vBar = np.sqrt(2.*f.dW(phiBar)/f.dCm(phiBar))
    a = -(D/p.L)*(p.tc/p.L)*lapEigens - p.tc*alpha*f.ddW(phiBar,a1) + p.tc*alpha/2.*f.ddCm(phiBar)*vBar**2
    b = p.tc*alpha*f.dCm(phiBar)*vBar
    c = -p.tc*f.dSm(phiBar,sigmoidCoeff,sigmoidCutOff,smModelWeight,ol)*vBar/f.Cm(phiBar)
    d = -p.tc/f.Cm(phiBar)*(f.Sm(phiBar,sigmoidCoeff,sigmoidCutOff,smModelWeight,ol) + lambdaEigens)
    trA = a + d
    detA = a*d - b*c
    return 0.5*(trA + np.emath.sqrt(trA**2 - 4.*detA)).astype(float)
def eigenMinus(a1,alpha,sigmoidCoeff,sigmoidCutOff,smModelWeight,ol,D,phiBar):
    vBar = np.sqrt(2.*f.dW(phiBar)/f.dCm(phiBar))
    a = -(D/p.L)*(p.tc/p.L)*lapEigens - p.tc*alpha*f.ddW(phiBar,a1) + p.tc*alpha/2.*f.ddCm(phiBar)*vBar**2
    b = p.tc*alpha*f.dCm(phiBar)*vBar
    c = -p.tc*f.dSm(phiBar,sigmoidCoeff,sigmoidCutOff,smModelWeight,ol)*vBar/f.Cm(phiBar)
    d = -p.tc/f.Cm(phiBar)*(f.Sm(phiBar,sigmoidCoeff,sigmoidCutOff,smModelWeight,ol) + lambdaEigens)
    trA = a + d
    detA = a*d - b*c
    return 0.5*(trA - np.emath.sqrt(trA**2 - 4.*detA)).astype(float)
def detAn(a1,alpha,sigmoidCoeff,sigmoidCutOff,smModelWeight,ol,D,phiBar):
    vBar = np.sqrt(2.*f.dW(phiBar)/f.dCm(phiBar))
    a = -(D/p.L)*(p.tc/p.L)*lapEigens - p.tc*alpha*f.ddW(phiBar,a1) + p.tc*alpha/2.*f.ddCm(phiBar)*vBar**2
    b = p.tc*alpha*f.dCm(phiBar)*vBar
    c = -p.tc*f.dSm(phiBar,sigmoidCoeff,sigmoidCutOff,smModelWeight,ol)*vBar/f.Cm(phiBar)
    d = -p.tc/f.Cm(phiBar)*(f.Sm(phiBar,sigmoidCoeff,sigmoidCutOff,smModelWeight,ol) + lambdaEigens)
    return a*d - b*c
def trAn(a1,alpha,sigmoidCoeff,sigmoidCutOff,smModelWeight,ol,D,phiBar):
    vBar = np.sqrt(2.*f.dW(phiBar)/f.dCm(phiBar))
    a = -(D/p.L)*(p.tc/p.L)*lapEigens - p.tc*alpha*f.ddW(phiBar,a1) + p.tc*alpha/2.*f.ddCm(phiBar)*vBar**2
    d = -p.tc/f.Cm(phiBar)*(f.Sm(phiBar,sigmoidCoeff,sigmoidCutOff,smModelWeight,ol) + lambdaEigens)
    return a + d
#=- initial-Values -=#
a10=p.a1
sigmoidCoeff0 = 12.5
sigmoidCutOff0 = 0.4
smModelWeight0 = 1e-3
alpha0 = p.alpha
ol0 = p.ol
D0 = p.D#To get 1e-12 multiply by 0.125
phiBar0 = 0.2
vBar0 = np.sqrt(2.*f.dW(phiBar0,a10)/f.dCm(phiBar0))
E0 = (p.oe+p.oc)*(f.Sm(phiBar0,sigmoidCoeff0,sigmoidCutOff0,smModelWeight0,ol0) + p.l0)*vBar0/(2.*p.oe*p.oc)
X = eigenPlus(a10,alpha0,sigmoidCoeff0,sigmoidCutOff0,smModelWeight0,ol0,D0,phiBar0)
Y = eigenMinus(a10,alpha0,sigmoidCoeff0,sigmoidCutOff0,smModelWeight0,ol0,D0,phiBar0)
P = detAn(a10,alpha0,sigmoidCoeff0,sigmoidCutOff0,smModelWeight0,ol0,D0,phiBar0)
Q = trAn(a10,alpha0,sigmoidCoeff0,sigmoidCutOff0,smModelWeight0,ol0,D0,phiBar0)
Z = np.sqrt(p.ij[eigenStart:eigenEnd:eigenStep])
eigen0_str = " eigen0 = ({},".format(round(X[0])) + " {})".format(round(Y[0]))
#----- Create the figure  we will manipulate ------#
cbshrink = 1.#default setting
location = "right"
aspect = 40
dotSize = 2.
# colobarbar properties
eigenCmap = cm.magma # cm.seismic# cm.cool
# colorbar label
eigenLabel=r"$|\vec{n}|$"
# plotting
fig, ax = plt.subplots(1,2,figsize=(20.,10.))
fig.suptitle("Stability Analysis Widget")
ax[0].set_xlabel(r'$Re(\lambda^{+})$')
ax[0].set_ylabel(r'$Re(\lambda^{-})$')
ax[0].grid()
ax[1].set_xlabel(r'$det(A)$')
ax[1].set_ylabel(r'$Tr(A)$')
ax[1].grid()
imEigens = ax[0].scatter(X,Y,c=Z,cmap=eigenCmap,s=dotSize)
imTraceDet = ax[1].scatter(P,Q,c=Z,cmap=eigenCmap,s=dotSize)
fig.colorbar(imEigens,ax=ax[1],location=location,shrink=cbshrink,label=eigenLabel,aspect=aspect)
ax[0].add_artist(lines.AxLine((0., 0.),(0.,1.),None, c='r'))
ax[0].add_artist(lines.AxLine((0., 0.),(1.,0.),None, c='r'))
ax[0].set_title("vBar = {} [V]".format(round(vBar0,2)) + eigen0_str)
ax[1].add_artist(lines.AxLine((0., 0.),(0.,1.),None, c='r'))
ax[1].add_artist(lines.AxLine((0., 0.),(1.,0.),None, c='r'))
ax[1].set_title("E = {}e5 [V/m]".format(round(E0*1e-5,1)))
# adjust the main plot to make room for the sliders
fig.subplots_adjust(bottom=0.5)
# Make a horizontal slider to control the Sm steepness: SmModelWeight
axSmModelWeight = fig.add_axes([0.2, 0.05, 0.5, 0.02])
smModelWeight_slider = Slider(
    ax=axSmModelWeight,
    label='Sm model weight ',
    valmin=np.log10(1e-6),
    valmax=np.log10(1.),
    valinit=np.log10(smModelWeight0),
)
# Make a horizontal slider to control the Sm steepness: SmCoeff
axSmCoeff = fig.add_axes([0.2, 0.1, 0.5, 0.02])
sigmoidCoeff_slider = Slider(
    ax=axSmCoeff,
    label='Sm steepness ',
    valmin=5.,
    valmax=20.,
    valinit=sigmoidCoeff0,
)
# Make a horizontal slider to control the Sm steepness: SmCutOff
axSmCutOff = fig.add_axes([0.2, 0.15, 0.5, 0.02])
sigmoidCutOff_slider = Slider(
    ax=axSmCutOff,
    label='Sm cutOff',
    valmin= 0.1,
    valmax=0.9,
    valinit=sigmoidCutOff0,
)
# Make a horizontal slider to control the lipid conductivity: ol
axOl = fig.add_axes([0.2, 0.20, 0.5, 0.02])
ol_slider = Slider(
    ax=axOl,
    label=r"$log_{10}(\sigma_{l})$",
    valmin=-7,
    valmax=-4,
    valinit= np.log10(ol0),
)
# Make a horizontal slider to control the kinetic coefficient: alpha
axAlpha = fig.add_axes([0.2, 0.25, 0.5, 0.02])
alpha_slider = Slider(
    ax=axAlpha,
    label=r"$\log_{10}(\alpha)$",
    valmin=np.log10(alpha0/20.),
    valmax= np.log10(alpha0*20.),
    valinit= np.log10(alpha0),
)
# Make a horizontal slider to control the diffusion coefficient: D
axD = fig.add_axes([0.2, 0.3, 0.5, 0.02])
D_slider = Slider(
    ax=axD,
    label=r"$log_{10}(D)$",
    valmin = np.log10(D0) - np.log10(8.),
    valmax = np.log10(D0) + np.log10(10./8.),
    valinit = np.log10(D0),
)
# Make a horizontal slider to control the value of phiBar
axA1 = fig.add_axes([0.2, 0.35, 0.5, 0.02])
a1_slider = Slider(
    ax=axA1,
    label=r"$a_{1}$",
    valmin = a10/20.,
    valmax = a10*20.,
    valinit = a10,
)
# Make a horizontal slider to control the value of phiBar
axPhiBar = fig.add_axes([0.2, 0.4, 0.5, 0.02])
phiBar_slider = Slider(
    ax=axPhiBar,
    label=r"$\bar{\phi}$",
    valmin = 0.,
    valmax = 0.4,
    valinit = phiBar0,
)
# The function to be called anytime a slider's value changes
def update(val):
    X = eigenPlus(a1_slider.val, 10.**alpha_slider.val, sigmoidCoeff_slider.val, sigmoidCutOff_slider.val, 10.**smModelWeight_slider.val, 10.**ol_slider.val, 10.**D_slider.val, phiBar_slider.val)
    Y = eigenMinus(a1_slider.val, 10.**alpha_slider.val,sigmoidCoeff_slider.val, sigmoidCutOff_slider.val, 10.**smModelWeight_slider.val, 10.**ol_slider.val, 10.**D_slider.val, phiBar_slider.val)
    P = detAn(a1_slider.val, 10.**alpha_slider.val,     sigmoidCoeff_slider.val, sigmoidCutOff_slider.val, 10.**smModelWeight_slider.val, 10.**ol_slider.val, 10.**D_slider.val, phiBar_slider.val)
    Q = trAn(a1_slider.val, 10.**alpha_slider.val,      sigmoidCoeff_slider.val, sigmoidCutOff_slider.val, 10.**smModelWeight_slider.val, 10.**ol_slider.val, 10.**D_slider.val, phiBar_slider.val)
    XY = np.stack((X,Y)).T #new points to plot
    PQ = np.stack((P,Q)).T
    imEigens.set_offsets(XY)
    imTraceDet.set_offsets(PQ)
    vBar = np.sqrt(2.*f.dW(phiBar_slider.val, a1_slider.val)/f.dCm(phiBar_slider.val))
    E = (p.oe+p.oc)*(f.Sm(phiBar_slider.val, sigmoidCoeff_slider.val, sigmoidCutOff_slider.val, 10.**smModelWeight_slider.val, 10.**ol_slider.val) + p.l0)*vBar/(2.*p.oe*p.oc)
    eigen1_str = " eigen0 = ({},".format(round(X[0])) + " {})".format(round(Y[0]))
    ax[0].set_title("vBar = {} [V]".format(round(vBar,2)) + eigen1_str)
    ax[1].set_title("E = {}e5 [V/m]".format(round(E*1e-5,1)))
    fig.canvas.draw_idle()
# register the update function with each slider
sigmoidCoeff_slider.on_changed(update)
sigmoidCutOff_slider.on_changed(update)
ol_slider.on_changed(update)
D_slider.on_changed(update)
alpha_slider.on_changed(update)
phiBar_slider.on_changed(update)
a1_slider.on_changed(update)
smModelWeight_slider.on_changed(update)
# Create a `matplotlib.widgets.Button` to reset the sliders to initial values.
resetax = fig.add_axes([0.8, 0.9, 0.1, 0.04])
button = Button(resetax, 'Reset', hovercolor='0.975')
def reset(event):
    sigmoidCoeff_slider.reset()
    sigmoidCutOff_slider.reset()
    ol_slider.reset()
    alpha_slider.reset()
    D_slider.reset()
    phiBar_slider.reset()
    a1_slider.reset()
    smModelWeight_slider.reset()
    ax[1].set_title("E = {}1e5 [V/m]".format(round(E0*1e-5,1)))
    ax[0].set_title("vBar = {} [V]".format(round(vBar0,2)) + eigen0_str)
button.on_clicked(reset)
plt.show()
