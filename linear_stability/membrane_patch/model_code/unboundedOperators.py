import numpy as np
import model_code.parameters as p
from numpy.fft import fftfreq
from math import pi

#-===== EigenValues ======-# (Flat membrane)
i,j = np.meshgrid(p.nx*fftfreq(p.nx),p.ny*fftfreq(p.ny))
ij = np.unique(i**2 + j**2)

def laplaceEigens(eigenStart,eigenEnd,eigenStep):#2D-periodic-Laplacian
    return (2.*pi)**2*(ij[eigenStart:eigenEnd:eigenStep])

def lambdaEigens(eigenStart,eigenEnd,eigenStep,L=p.L,H=p.H,oe=p.oe,oc=p.oc):#2D-Steklov
    eigens = (2.*pi*H/L)*np.sqrt(ij[eigenStart:eigenEnd:eigenStep])
    eigens[eigens > 0.] = eigens[eigens > 0.]/np.tanh(eigens[eigens > 0.])
    eigens[eigens == 0.] = 1.
    return (1./H)*oc*oe/(oe+oc)*eigens

#---- Value is contained in lambdaEigens. Only defined for ease of use ----#
l0 = (1./p.H)*p.oc*p.oe/(p.oe+p.oc) 
