#-==== Physical constants ====-#
cellRadius = 5e-6# [m]
extraConductivity = 1.25# [S/m] [0.1, 1.5]
intraConductivity = 0.5# [S/m] 
lipidConductivity = 1e-5# [S/m]
membraneThickness = 5e-9# [m]
vacuumPermittivity = 8.85*1e-12# [F/m]
waterDielectricCste = 80.# [1]
lipidDielectricCste = 2.# [1]
lateralDiffusion = 8e-12# [m^2/s]
surfaceEnergy = 1e-6# [J/m^2]
linearEnergy = 1.8e-11# [J/m]
poreEdge = 1e-9# [m]

#-==== Characteristic Values ====-#
characteristicPatchLength = 2e-7# [m]
characteristicTime = 1e-6# [s]

#-==== Discretization Parameters ====-#
meshResolution = 1. #(should be >= 1)
nx = int((characteristicPatchLength/poreEdge)*meshResolution)
ny = nx

#-===== parameter notation ====-#
tc = characteristicTime
L  = characteristicPatchLength
e0 = vacuumPermittivity
ew = waterDielectricCste
el = lipidDielectricCste
h  = membraneThickness
D = lateralDiffusion
oe = extraConductivity
oc = intraConductivity
ol = lipidConductivity
ow = (oe+oc)/2.
H  = 0.5*cellRadius*(oc + 2.*oe)/(oc + oe)# [m] distance between electrodes is 2H
a1 = linearEnergy/poreEdge# [J/m^2]
a2 = surfaceEnergy# [J/m^2]
alpha = (D/poreEdge)/(4.*a1*poreEdge)# [m^2J^-1s^-1] (kinetic coefficient)

#----Parameter tuning-----#
D *= 1.
alpha *= 5.#
a1 *= 1.#
