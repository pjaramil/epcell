import model_code.parameters as p
import numpy as np
from math import pi

#-========== HELPER FUNCTIONS ==========-#
def sigmoidf(phi,s,r):
    return 0.5*(1.+ np.tanh(s*(phi-r)))
def dsigmoidf(phi,s,r):
    return  0.5*s*(1.- np.tanh(s*(phi-r))**2)
def ddsigmoidf(phi,s,r):
    return -s**2.*np.tanh(s*(phi-r))*(1.- np.tanh(s*(phi-r))**2)
#-========== MODEL FUNCTIONS ==========-#
#-==== Modeling options =====-#
sm_coeff = 10.
sm_cutOff = 0.5
smModelWeight = 1e-4
cm_tuning1 = 13.
cm_tuning2 = 12.
cm_cutOff = 1.

def Cm(phi,linearMixtureModel): 
    if linearMixtureModel:
        delta = p.ew - p.el
        c0 = p.el + phi*delta #linear model
    else:
        delta = (p.ew**(1./3.) - p.el**(1./3.))
        c0 = (p.el**(1./3.) + phi*delta)**3. # Looyenga
    return (p.e0/p.h)*(c0*sigmoidf(phi,-cm_tuning1,cm_cutOff) + p.ew*sigmoidf(phi,cm_tuning2,cm_cutOff))

def dCm(phi,linearMixtureModel):
    if linearMixtureModel:
        delta = p.ew - p.el
        c0 = p.el + phi*delta #linear model
        dc0 = delta
    else:
        delta = (p.ew**(1./3.) - p.el**(1./3.))
        c0 = (p.el**(1./3.) + phi*delta)**3. # Looyenga
        dc0 = 3.*delta*(p.el**(1./3.) + phi*delta)**2. # dLooyenga/dphi
    return (p.e0/p.h)*(dc0*sigmoidf(phi,-cm_tuning1,cm_cutOff) + c0*dsigmoidf(phi,-cm_tuning1,cm_cutOff) + p.ew*dsigmoidf(phi,cm_tuning2,cm_cutOff))

def ddCm(phi,linearMixtureModel):
    if linearMixtureModel:
        delta = p.ew - p.el
        c0 = p.el + phi*delta #linear model
        dc0 = delta
        ddc0 = 0.
    else:
        delta = (p.ew**(1./3.) - p.el**(1./3.))
        c0 = (p.el**(1./3.) + phi*delta)**3. # Looyenga
        dc0 = 3.*delta*(p.el**(1./3.) + phi*delta)**2. # dLooyenga/dphi
        ddc0 = 6.*delta**2*(p.el**(1./3.)+phi*delta)
    return (p.e0/p.h)*(ddc0*sigmoidf(phi,-cm_tuning1,cm_cutOff) + 2.*dc0*dsigmoidf(phi,-cm_tuning1,cm_cutOff) + c0*ddsigmoidf(phi,-cm_tuning1,cm_cutOff) + p.ew*ddsigmoidf(phi,cm_tuning2,cm_cutOff))
#------------ Sigmoid conductance model ------------#
def Sm(phi,ow=p.ow,ol=p.ol):
    return ((1.-smModelWeight)*sigmoidf(phi,sm_coeff,sm_cutOff)/p.h+smModelWeight/p.h*phi)*(ow-ol)+ol/p.h

def dSm(phi,ow=p.ow,ol=p.ol):
    return ((1.-smModelWeight)*dsigmoidf(phi,sm_coeff,sm_cutOff)+ smModelWeight)*(p.ow-ol)/p.h
#---------------- Membrane energy potential -----------#
def W(phi,a1=p.a1,a2=p.a2):
    return a1*phi**2*(phi-1.)**2 + a2*(phi + 0.5)*(phi-1.)**2

def dW(phi,a1=p.a1,a2=p.a2):
    return 4.*a1*phi*(phi-1.)*(phi - 0.5 + 3./4.*a2/a1)

def ddW(phi,a1=p.a1,a2=p.a2):
    return 4.*a1*( phi*(3.*phi - 2.) + (2.*phi - 1.)*(-0.5 + 3./4.*a2/a1) )


