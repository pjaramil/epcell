import model_code.parameters as p
import model_code.functions as f
import model_code.unboundedOperators as u
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as lines
from matplotlib.widgets import Button, Slider
from matplotlib import cm # colormaps
from math import pi
from numpy.linalg import det

#----------------------- Settings -----------------------#
eigenStart = 0
eigenEnd = 1000
eigenStep = 1
laplaceEigens = u.laplaceEigens(eigenStart,eigenEnd,eigenStep)
lambdaEigens = u.lambdaEigens(eigenStart,eigenEnd,eigenStep)
result_folder = "./graphs"
try:
    os.mkdir(result_folder)
except OSError as error:
    print("")

#=-------------------- Setup Values ------------------=#
phiBar_0 = np.array([0.1,0.15,0.2]) # stationnary solution
vBar_0_looyenga = np.sqrt(2.*f.dW(phiBar_0)/f.dCm(phiBar_0,False))# stationnary solution
vBar_0_linear = np.sqrt(2.*f.dW(phiBar_0)/f.dCm(phiBar_0,True))# stationnary solution
E_0_looyenga = (p.oe+p.oc)*(f.Sm(phiBar_0) + u.l0)*vBar_0_looyenga/(2.*p.oe*p.oc)# Electric field needed for the above solution
E_0_linear = (p.oe+p.oc)*(f.Sm(phiBar_0) + u.l0)*vBar_0_linear/(2.*p.oe*p.oc)# Electric field needed for the above solution
#--------------------------------------------------------#

#-------------------- Matrix entries --------------------#
#                  A = [ a  b ]
#                      [ c  d ]
#a = (D*tc/L**2)*laplaceEigens - tc*alpha*f.ddW(phiBar) + tc*alpha/2.*f.ddCm(phiBar)*vBar**2
#b = tc*alpha*f.dCm(phiBar)*vBar
#c = -tc*f.dSm(phiBar)*vBar/f.Cm(phiBar)
#d = -tc/f.Cm(phiBar)*(f.Sm(phiBar) + lambdaEigens)
#--------------------------------------------------------#

#----------------- Eigenvalues --------------------------#
def eigenPlus(phiBar,linearMixtureModel):
    vBar = np.sqrt(2.*f.dW(phiBar)/f.dCm(phiBar,linearMixtureModel))
    a = -(p.D/p.L)*(p.tc/p.L)*laplaceEigens - p.tc*p.alpha*f.ddW(phiBar) + p.tc*p.alpha/2.*f.ddCm(phiBar,linearMixtureModel)*vBar**2
    b = p.tc*p.alpha*f.dCm(phiBar,linearMixtureModel)*vBar
    c = -p.tc*f.dSm(phiBar)*vBar/f.Cm(phiBar,linearMixtureModel)
    d = -p.tc/f.Cm(phiBar,linearMixtureModel)*(f.Sm(phiBar) + lambdaEigens)
    trA = a + d
    detA = a*d - b*c
    return 0.5*(trA + np.emath.sqrt(trA**2 - 4.*detA)).astype(float)

def eigenMinus(phiBar,linearMixtureModel):
    vBar = np.sqrt(2.*f.dW(phiBar)/f.dCm(phiBar,linearMixtureModel))
    a = -(p.D/p.L)*(p.tc/p.L)*laplaceEigens - p.tc*p.alpha*f.ddW(phiBar) + p.tc*p.alpha/2.*f.ddCm(phiBar,linearMixtureModel)*vBar**2
    b = p.tc*p.alpha*f.dCm(phiBar,linearMixtureModel)*vBar
    c = -p.tc*f.dSm(phiBar)*vBar/f.Cm(phiBar,linearMixtureModel)
    d = -p.tc/f.Cm(phiBar,linearMixtureModel)*(f.Sm(phiBar) + lambdaEigens)
    trA = a + d
    detA = a*d - b*c
    return 0.5*(trA - np.emath.sqrt(trA**2 - 4.*detA)).astype(float)
#--------------------------------------------------------#


#-------------------- Plotting ---------------------#

#--------------- data -----------------#
Z = np.sqrt(u.ij[eigenStart:eigenEnd:eigenStep])

#--------------- creating plot -----------------#
fig, ax = plt.subplots(1,1,layout='constrained')
fig.tight_layout()
#plotting
imEigens_linear_0 = ax.scatter(eigenPlus(phiBar_0[0],True),eigenMinus(phiBar_0[0],True),c=Z,marker='.',cmap=cm.magma,s=10,label=r"Linear, $\bar{\phi} = $" + f"{phiBar_0[0]:.02f}")
imEigens_linear_1 = ax.scatter(eigenPlus(phiBar_0[1],True),eigenMinus(phiBar_0[1],True),c=Z,marker='o',cmap=cm.magma,s=10,label=r"Linear, $\bar{\phi} = $" + f"{phiBar_0[1]:.02f}")
imEigens_linear_2 = ax.scatter(eigenPlus(phiBar_0[2],True),eigenMinus(phiBar_0[2],True),c=Z,marker='s',cmap=cm.magma,s=10,label=r"Linear, $\bar{\phi} = $" + f"{phiBar_0[2]:.02f}")
imEigens_looyenga_0 = ax.scatter(eigenPlus(phiBar_0[0],False),eigenMinus(phiBar_0[0],False),marker=4,c=Z,cmap=cm.brg_r,s=20,label=r"Looyenga, $\bar{\phi} = $" + f"{phiBar_0[0]:.02f}")
imEigens_looyenga_1 = ax.scatter(eigenPlus(phiBar_0[1],False),eigenMinus(phiBar_0[1],False),marker=5,c=Z,cmap=cm.brg_r,s=20,label=r"Looyenga, $\bar{\phi} = $" + f"{phiBar_0[1]:.02f}")
imEigens_looyenga_2 = ax.scatter(eigenPlus(phiBar_0[2],False),eigenMinus(phiBar_0[2],False),marker=6,c=Z,cmap=cm.brg_r,s=20,label=r"Looyenga, $\bar{\phi} = $" + f"{phiBar_0[2]:.02f}")
#adding colorbar
cb_linear = fig.colorbar(imEigens_linear_0,ax=ax,location="right",shrink=0.8,aspect=80)
cb_looyenga = fig.colorbar(imEigens_looyenga_0,ax=ax,location="left",shrink=0.8,aspect=80)
#vertical and horizontal red lines
ax.add_artist(lines.AxLine((0., 0.),(0.,1.),None, linewidth=1.5, c='r'))
ax.add_artist(lines.AxLine((0., 0.),(1.,0.),None, linewidth=1.5, c='r'))

#------------ plot settings --------------#
cb_looyenga.set_label(label=r"$|\vec{k}|$ (Looyenga)",fontsize='x-large')
cb_linear.set_label(label=r"$|\vec{k}|$ (Linear)",fontsize='x-large')
ax.set_xlabel(r'$Re(\lambda_{\vec{k}}^{+})$', fontsize='x-large')
ax.set_ylabel(r'$Re(\lambda_{\vec{k}}^{-})$', fontsize='x-large')
ax.set_yscale('symlog')
#ax.set_aspect('auto')
ax.grid()
ax.set_yticks([1e-1,-1e1,-1e2, -1e3,-1e4])
ax.set_title("Mixture models comparison",fontsize='xx-large')
ax.legend()
plt.show()

#-------------------- Saving setup ----------------#
fig.savefig(result_folder+"/ComparingMixtureModels.pdf")
info_file = open(result_folder+"/stability_info.txt", "w")
info_file.write("In the linear model graph, these are the parameters used :\n")
info_file.write("phiBar_1 = " + str(phiBar_0[0]) + "\n")
info_file.write("vBar_1 = " + str(vBar_0_linear[0]) + "[V]\n")
info_file.write("E_1 = "+str(E_0_linear[0]*1e-5)+".1e5 [V/m]\n")
info_file.write("phiBar_2 = " + str(phiBar_0[1])+ "\n")
info_file.write("vBar_2 = " + str(vBar_0_linear[1]) + "[V]\n")
info_file.write("E_2 = "+str(E_0_linear[1]*1e-5)+".1e5 [V/m]\n")
info_file.write("phiBar_3 = " + str(phiBar_0[2])+ "\n")
info_file.write("vBar_3 = " + str(vBar_0_linear[2]) + "\n")
info_file.write("E_3 = "+str(E_0_linear[2]*1e-5)+".1e5 [V/m]\n")
info_file.write("In the Looyenga model graph, these are the parameters used :\n")
info_file.write("phiBar_1 = " + str(phiBar_0[0])+ "\n")
info_file.write("vBar_1 = " + str(vBar_0_looyenga[0])+"[V]\n")
info_file.write("E_1 = "+str(E_0_looyenga[0]*1e-5)+".1e5 [V/m]\n")
info_file.write("phiBar_2 = " + str(phiBar_0[1])+ "\n")
info_file.write("vBar_2 = " + str(vBar_0_looyenga[1])+"[V]\n")
info_file.write("E_2 = "+str(E_0_looyenga[1]*1e-5)+".1e5 [V/m]\n")
info_file.write("phiBar_3 = " + str(phiBar_0[2])+ "\n")
info_file.write("vBar_3 = " + str(vBar_0_looyenga[2])+"[V]\n")
info_file.write("E_3 = "+str(E_0_looyenga[2]*1e-5)+".1e5 [V/m]\n")
info_file.close()
