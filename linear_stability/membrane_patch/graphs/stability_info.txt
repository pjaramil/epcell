In the linear model graph, these are the parameters used :
phiBar_1 = 0.1
vBar_1 = 0.19376527054587206[V]
E_1 = 0.39541342706655824.1e5 [V/m]
phiBar_2 = 0.15
vBar_2 = 0.2157300114884239[V]
E_2 = 0.7471254432294027.1e5 [V/m]
phiBar_3 = 0.2
vBar_3 = 0.22373696658301123
E_3 = 1.6334852065923782.1e5 [V/m]
In the Looyenga model graph, these are the parameters used :
phiBar_1 = 0.1
vBar_1 = 0.36159659287919604[V]
E_1 = 0.7379038957969755.1e5 [V/m]
phiBar_2 = 0.15
vBar_2 = 0.36684728215964857[V]
E_2 = 1.2704812667927587.1e5 [V/m]
phiBar_3 = 0.2
vBar_3 = 0.3494418652115719[V]
E_3 = 2.5512463412047217.1e5 [V/m]
