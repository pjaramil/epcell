import numpy as np
import parameters as par
from numpy.fft import fftfreq
from math import pi

#-===== EigenValues ======-#
def laplaceEigens(Nx=par.nx,Ny=par.ny): # 2D-periodic-Laplacian eigenvalues
    i,j = np.meshgrid(Nx*fftfreq(Nx),Ny*fftfreq(Ny))
    return (2.*pi)**2*(i**2 + j**2)

def lambdaEigens(Nx=par.nx,Ny=par.ny,L=par.L,H=par.H,oe=par.oe,oc=par.oc): # 2D-Steklov eigenvalues
    i,j = np.meshgrid(Nx*fftfreq(Nx),Ny*fftfreq(Ny))
    eigens = (2.*pi*H/L)*np.sqrt(i**2 + j**2)
    eigens[np.nonzero(eigens)] = eigens[np.nonzero(eigens)]/np.tanh(eigens[np.nonzero(eigens)])
    eigens[0,0] = 1.
    return (1./H)*oc*oe/(oe+oc)*eigens

#-===== EigenValue arrays ======-#
lapEigens = laplaceEigens()
l0 = 0. #(1./H)*oc*oe/(oe+oc)
lambdaEigens0 = lambdaEigens() - l0
