import parameters as par
import unboundedOperators as u
import functions as f
import numpy as np
from numba import jit, njit

#-==========- FUNCTIONS -==========-#
@njit
def odeF(t,phi,v):# psi = (phi,V)
    fv   = -par.tc*(f.Sm(phi) + u.l0)*v/f.Cm(phi)
    fv   += par.tc*f.G(t)/f.Cm(phi)
    return fv

@njit
def ode_timeStep(dt,t,phin,vn): #Heun's method:
    v2 = vn + dt*odeF(t,phin,vn)
    return vn + 0.5*dt*(odeF(t,phin,vn) + odeF(t+dt,phin,v2))
