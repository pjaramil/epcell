import numpy as np
import parameters as par
import functions as f
import unboundedOperators as u
from numpy.fft import fft2, ifft2
from numpy.linalg import norm
from numba import jit, njit

#===============================================================================#
#============================== PDE SPLIT FOR TMV ==============================#
#===============================================================================#

#- preconditioner parameters
phi_x = 0.5 # mid point between the two extreme values of phi
cste = 0.07 #0.1 # supposed to be about mean(Cm(phis)/Cm(phi_x)) ~ i.e. somewhere close to 1/20 \in [1/20, 2]
#- CG_FFT_Solver parameters
tol_CG = max(1e1*par.nx/par.nt**3, 5e-8)# to stop iterative algorithm
maxIteration_CG = 100

@njit
def B_rhs(t,dt,vn,phin):
    B =  vn - 0.5*dt*par.tc*ifft2(u.lambdaEigens0*fft2(vn))/f.Cm(phin)
    B *= np.sqrt(f.Cm(phin))
    B =  fft2(B)
    B /= np.sqrt(cste + (0.5*par.tc*dt/f.Cm(phi_x))*u.lambdaEigens0)
    B =  ifft2(B)
    B *= np.sqrt(f.Cm(phin)/f.Cm(phi_x))
    return B

@njit
def A_lhs(dt,phin,X):
    fn = np.sqrt(f.Cm(phin))
    #First half of the preconditionner (it is symmetric)
    X0 = fn*X/(f.Cm(phi_x))**0.5
    X0 = fft2(X0)/np.sqrt(cste + (0.5*par.tc*dt/f.Cm(phi_x))*u.lambdaEigens0)
    X0 = ifft2(X0)
    # Applying the unconditioned operator A
    X1 = ifft2(u.lambdaEigens0*fft2(X0/fn))/fn
    X1 = X0 + 0.5*par.tc*dt*X1
    # Applying the last bit of the preconditionner
    X1 = fft2(X1)/np.sqrt(cste + (0.5*par.tc*dt/f.Cm(phi_x))*u.lambdaEigens0)
    X1 = fn*ifft2(X1)/(f.Cm(phi_x))**0.5
    return X1

@njit
def A_CG_FFT_Solver(dt,phin,B,showIter=False):
    #Solve linear system Ax = B
    x = 0.*B #x0 = 0 : initial guess
    r = B # - A_lhs(x) <- This part was removed because x0 = 0
    p = r
    gamma = norm(r)
    iter = 0
    while gamma > tol_CG and iter < maxIteration_CG:
        y = A_lhs(dt,phin,p)
        alpha = gamma**2/np.sum(y*np.conjugate(p))
        x = x + alpha*p
        r = r - alpha*y
        beta = norm(r)**2/gamma**2
        gamma = norm(r)
        p = r + beta*p
        iter = iter + 1
    if showIter or iter >= maxIteration_CG:
        print("CG-iteration =", iter)
    return x

@njit
def v_pde_timeStep(t,dt,phin,vn,showInfo):
    B = B_rhs(t,dt,vn,phin)
    Y = A_CG_FFT_Solver(dt,phin,B,showIter=showInfo)
    return ifft2(fft2(Y*np.sqrt(f.Cm(phin)/f.Cm(phi_x)))/np.sqrt(cste + (0.5*par.tc*dt/f.Cm(phi_x))*u.lambdaEigens0))/np.sqrt(f.Cm(phin))

#------ PDE SPLIT --------#
@njit
def pde_timeStep(dt,t,phin,vn,showInfo):# psin = np.array([phin,vn])
    return v_pde_timeStep(t,dt,phin,vn,showInfo)
