import parameters as par
import organize as org
import functions as f
import initialCondition as ic
import PDE_timeStep as pde
import ODE_timeStep as ode
import plotting as plt
import numpy as np
import time
import glob
from numpy.linalg import norm

#-===== Simulation Parameters =====-#
showInfo = True
showAnimation = False
saveAnimation = True
flatPlot = True
surfacePlot = True
solution_savePeriod = (1e-9/par.tc) # [tc]
mean_savePeriod = (1e-9/par.tc) # [tc]
dt = 1./par.nt# [tchar]
t = 0. # [tc]
pore_radius = 0.15

#======= create folders & copy files =======#
cpu_time = time.time_ns()
fname = str(cpu_time)[3:-6]
folder = "/home/pedrojaramillo/Documents/Code/work/numericalResults/"
folder +=  fname + "_tmv_patch"
org.setup_folders(filePath=folder)

#======= INITIALIZATION =======#
phin = ic.explicit_four_smooth_hole_phi(par.nx,par.ny,pore_radius)
vn = np.zeros((par.nx,par.ny))

#===== Saving initial conditions ======#
solution_timer = [0.]
mean_timer = [0.]
#----------------- solution -----------------#
np.save(folder+"/data/phi_{:0>5}".format(0), phin)
np.save(folder+"/data/v_{:0>5}".format(0), vn)
#----------------- mean behavior -----------------#
mean_phi = [f.mean(phin)]
mean_v = [f.mean(vn)]

#=====================================================#
#===================== SIMULATION ====================#
while (t < par.tf):
    if showInfo:
        print("------------------------")
        print("t = ", t*par.tc*1e9, " [ns]")
        print("L = ", par.L*1e9, " [nm]")
        print("E = ", par.E, " [V/m]")
    #------ Strang Splitting ------#
    v_star  = ode.ode_timeStep(dt/2.,t,phin,vn)
    v_star_star = pde.pde_timeStep(dt,t,phin,v_star,showInfo)
    v       = ode.ode_timeStep(dt/2.,t+dt/2.,phin,v_star_star)
    #------ Updating ------#
    t = t + dt
    vn = v
    #------ Saving/Accumulating ------#
    if (t > mean_timer[-1] + mean_savePeriod):
        mean_v.append(f.mean(vn))
        mean_timer.append(mean_timer[-1]+mean_savePeriod)
    if (t > solution_timer[-1] + solution_savePeriod):
        np.save(folder+"/data/v_{:0>5}".format(len(solution_timer)),vn)
        solution_timer.append(solution_timer[-1]+solution_savePeriod)
#================= END OF SIMULATION =================#
#=====================================================#

#================= Saving mean evolution =================#
#---- from list to arrays ----#
solution_timer = np.array(solution_timer)*par.tc*1e9 #[ns]
mean_timer = np.array(mean_timer)*par.tc*1e9 #[ns]
mean_v = np.array(mean_v)
#---- Saving ----#
np.save(folder+"/data/solution_timer", solution_timer)
np.save(folder+"/data/mean_timer", mean_timer)
np.save(folder+"/data/mean_v", mean_v)


#================= Plotting =================#
#---------- electric properties ----------#
plt.plot_CmSm_models(folder+"/graphs")
#----------------- solution -----------------#
#--- Getting the data ---#
phi_files = glob.glob(folder+"/data/phi_*.npy")
v_files = glob.glob(folder+"/data/v_*.npy")
#--- arranging ---#
phi_files.sort()
v_files.sort()
#--- to arrays ---#
Zvs = []
for file in v_files:
    Zvs.append(np.load(file))
Zvs = np.array(Zvs).astype(float)

#--- solution plotting ---#
if flatPlot:
    plt.plotFlat(phin.astype(float),title=r'$\phi$',barlabel='[1]',fPath=folder+"/graphs/phi_flat.pdf",show=showAnimation)
    plt.animateFlat(Zvs,solution_timer, title=r'TMV',barlabel='[1]',fPath=folder+"/graphs/v_flat.gif",color='cool',show=showAnimation)
if surfacePlot:
    plt.plot3D(phin.astype(float),title=r'$\phi$',barlabel='[1]',fPath=folder+"/graphs/phi_3D.pdf",show=showAnimation)
    plt.animate3D(Zvs,solution_timer, title=r'TMV',barlabel='[1]',fPath=folder+"/graphs/v_3D.gif",color='cool',show=showAnimation)

#--- mean behavior plotting ---#
plt.plot_graph(mean_v,mean_timer,folder+"/graphs/mean_v.pdf",r"$\frac{1}{|\Gamma|}\int_{\Gamma}V\,dx$",r"$t~[ns]$",r"[V]")
